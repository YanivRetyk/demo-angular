import { ImageService } from './../image.service';
import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-rekognition',
  templateUrl: './rekognition.component.html',
  styleUrls: ['./rekognition.component.css']
})
export class RekognitionComponent implements OnInit {

  fileObj: File;
  fileUrl: string;
  errorMsg: boolean;
  title = 's3-file-uploader-app';

  constructor(private imageService: ImageService,
    private router: Router) { 
    this.errorMsg = false;
  }

  onFilePicked(event: Event): void {

    this.errorMsg = false;
    console.log(event);
    const FILE = (event.target as HTMLInputElement).files[0];
    this.fileObj = FILE;
    console.log(this.fileObj);
  }

  onFileUpload() {

    if (!this.fileObj) {
      this.errorMsg = true;
      return
    }
    const fileForm = new FormData();
    fileForm.append('file', this.fileObj);
    this.imageService.fileUpload(fileForm).subscribe(res => {
      console.log(res);
      this.fileUrl = res['image'];

    });
    this.router.navigate(['/rekdisp/'])
  }

  ngOnInit() {
  }

}
