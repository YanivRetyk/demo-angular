import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {
  
  private url = "https://qqoyl8b581.execute-api.us-east-1.amazonaws.com/beta";
  //private url = "https://rniqbt3fbh.execute-api.us-east-1.amazonaws.com/beta";
  //private url = "https://a38dg0j0zj.execute-api.us-east-1.amazonaws.com/beta";
  
  
  //public categories:object = {0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'}
  public categories:object = {
  0: 'ARSON',
 1: 'ASSAULT',
 2: 'BAD CHECKS',
 3: 'BRIBERY',
 4: 'BURGLARY',
 5: 'DISORDERLY CONDUCT',
 6: 'DRIVING UNDER THE INFLUENCE',
 7: 'DRUG/NARCOTIC',
 8: 'DRUNKENNESS',
 9: 'EMBEZZLEMENT',
 10: 'EXTORTION',
 11: 'FAMILY OFFENSES',
 12: 'FORGERY/COUNTERFEITING',
 13: 'FRAUD',
 14: 'GAMBLING',
 15: 'KIDNAPPING',
 16: 'LARCENY/THEFT',
 17: 'LIQUOR LAWS',
 18: 'LOITERING',
 19: 'MISSING PERSON',
 20: 'NON-CRIMINAL',
 21: 'OTHER OFFENSES',
 22: 'PORNOGRAPHY/OBSCENE MAT',
 23: 'PROSTITUTION',
 24: 'RECOVERED VEHICLE',
 25: 'ROBBERY',
 26: 'RUNAWAY',
 27: 'SECONDARY CODES',
 28: 'SEX OFFENSES, FORCIBLE',
 29: 'SEX OFFENSES, NON FORCIBLE',
 30: 'STOLEN PROPERTY',
 31: 'SUICIDE',
 32: 'SUSPICIOUS OCC',
 33: 'TREA',
 34: 'TRESPASS',
 35: 'VANDALISM',
 36: 'VEHICLE THEFT',
 37: 'WARRANTS',
 38: 'WEAPON LAWS'
  }
  public doc:string; 
  
  classify():Observable<any>{
    console.log(this.doc);
    let json = {
      "crimes": [
        {
          "text": this.doc
        }
      ]
    }
    /*
    let json = {
      "articles": [
        {
          "text": this.doc
        },
      ]
    }
    */
    let body  = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        console.log(res.body);
        let final = res.body.replace('[','');
        final = final.replace(']','');
        console.log(final);
        return final;      
      })
    );      
  }

  constructor(private http: HttpClient) { }
}
