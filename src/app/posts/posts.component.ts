import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Post } from '../interfaces/post';
import { Photos } from '../interfaces/photos';
import { Crime } from '../interfaces/crime';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  
  //geting posts from json not from db
  posts$: Post[];
  photos$: Photos[];
  crimes$: Crime[];

  myFunc(comp:boolean, title:string){
  
 this.postsService.addPhoto(comp,title);

}

myFuncCri(id){
  
  this.postsService.goto(id);
 
 }


  constructor(private postsService: PostsService) { }
  ngOnInit() {
    //geting posts from json
        this.postsService.getPosts().subscribe(data => this.posts$ = data)
        this.postsService.getPhotos().subscribe(data => this.photos$ = data)
        this.postsService.getCrime().subscribe(data => this.crimes$ = data)
  }

}