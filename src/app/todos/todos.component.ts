import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TodosService } from '../todos.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  constructor(public  todosService : TodosService) { }

  todos$:Observable<any[]>;


  ngOnInit() {
    this.todos$ = this.todosService.getTodos();
  }

  

}
