import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClassifiedService {

  constructor(private db: AngularFirestore,
    private router:Router) { }
  
  
getArticles():Observable<any[]>{
  return this.db.collection('articles').valueChanges({idField:'id'});
}


deleteArticle(id:string){
  this.db.doc(`articles/${id}`).delete();
}

  addArticle(category:String,  body:String){
    const article = {category:category, body:body}
    this.db.collection('articles').add(article)  
    .then(
      res =>  
      {
    this.router.navigate(['/articles']);
      }
    )
  }  

}
