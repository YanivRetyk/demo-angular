import { HttpClient, HttpParams } from '@angular/common/http';
//import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ImageService } from './../image.service';
import { RekdispService } from './../rekdisp.service';
import { ClassifyService } from './../classify.service';
import { ClassifiedService } from './../classified.service';
import { Rek } from '../interfaces/rek';

@Component({
  selector: 'app-rekdisp',
  templateUrl: './rekdisp.component.html',
  styleUrls: ['./rekdisp.component.css']
})
export class RekdispComponent implements OnInit {

  body:string;
  reks$: Rek[];


  constructor (public rekdispService:RekdispService,
  public imageService:ImageService,
  public classifiedService:ClassifiedService,
  private http: HttpClient) { }

  ngOnInit() {
    const params = new HttpParams()
        .set("page","1")
        .set("pageSize","10");
    
//this.reks$ = this.http.get<Rek[]>('http://ec2-18-188-71-144.us-east-2.compute.amazonaws.com/rekognition',{params});



    this.rekdispService.getRek().subscribe(data => this.reks$ = data)
    
    this.rekdispService.rekog().subscribe(
       res => {
        console.log(res);
        console.log(this.rekdispService[res])
        this.body = res;
        console.log(this.rekdispService.doc)
       
       }
    )
  }

}
