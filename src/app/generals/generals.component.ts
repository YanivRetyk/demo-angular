import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { GeneralsService } from '../generals.service';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-generals',
  templateUrl: './generals.component.html',
  styleUrls: ['./generals.component.css']
})
export class GeneralsComponent implements OnInit {

  panelOpenState = false;
  //generals: object[] =  [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]
  //generals: object[];
  generals$:Observable<any[]>;
  userId:string;

  deleteGeneral(id){
    this.generalservice.deleteGeneral(this.userId,id)
    console.log(id);
  }
  
  constructor(private generalservice:GeneralsService,
              public authService:AuthService,
              public datepipe: DatePipe) { }

 myFunction(date:Date){
     date=new Date();
    return this.datepipe.transform(date, 'yyyy-MM-dd');
    }

  ngOnInit() { 
    console.log("NgOnInit started")  
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.generals$ = this.generalservice.getGenerals(this.userId); 
      }
    )
  }
}

