import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  public images:string[] = [];
  public path:string = 'https://firebasestorage.googleapis.com/v0/b/ex2019-9242c.appspot.com/o/'
 
constructor(private http : HttpClient) {
  this.images[0] = this.path +'biz.JPG' + '?alt=media';
  this.images[1] = this.path +'entermnt.JPG' + '?alt=media';
  this.images[2] = this.path +'politics-icon.png' + '?alt=media';
  this.images[3] = this.path +'sport.JPG' + '?alt=media';
  this.images[4] = this.path +'tech.JPG' + '?alt=media';
}


fileUpload(file: FormData) {
  return this.http.post('http://ec2-18-188-71-144.us-east-2.compute.amazonaws.com/upload-ng', file);
}

}
