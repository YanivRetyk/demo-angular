import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Force } from '../interfaces/force';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-station',
  templateUrl: './station.component.html',
  styleUrls: ['./station.component.css']
})
export class StationComponent implements OnInit {

  //geting posts from json not from db
  forces: any;

  id:String;

  constructor(private postsService: PostsService,
      private authService:AuthService, 
      private router:Router,
      private route: ActivatedRoute) { }

  ngOnInit() {
    //geting posts from json
        this.id = this.route.snapshot.params.id;
        this.postsService.goto(this.id).subscribe(data => this.forces = data)
  }

}