import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { GeneralsComponent } from './generals/generals.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule, MatNativeDateModule } from '@angular/material';

import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { GeneralformComponent } from './generalform/generalform.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { DocformComponent } from './docform/docform.component';
import { ClassifiedComponent } from './classified/classified.component';
import { HttpClientModule } from '@angular/common/http';
import { WelcomComponent } from './welcom/welcom.component';
import { ArticlesComponent } from './articles/articles.component';
import { PostsComponent } from './posts/posts.component'; 
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';
import { DatePipe } from '@angular/common';
import { TodosComponent } from './todos/todos.component';
import { StationComponent } from './station/station.component';
import { RekognitionComponent } from './rekognition/rekognition.component';
import { RekdispComponent } from './rekdisp/rekdisp.component';


const appRoutes: Routes = [
    { path: 'welcom', component: WelcomComponent },
    { path: 'generals', component: GeneralsComponent },
    { path: 'generalform', component: GeneralformComponent},
    { path: 'generalform/:id', component: GeneralformComponent},
    { path: 'signup', component: SignUpComponent},
    { path: 'login', component: LoginComponent},
    { path: 'classify', component: DocformComponent},
    { path: 'articles', component: ArticlesComponent},
    { path: 'posts', component: PostsComponent},
    { path: 'classified', component: ClassifiedComponent},
    { path: 'rekognition', component: RekognitionComponent},
    { path: 'rekdisp', component: RekdispComponent},
    { path: 'todos', component: TodosComponent },
    { path: 'station/:id', component: StationComponent },
    { path: '',
      redirectTo: '/generals',
      pathMatch: 'full'
    },
  ];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    GeneralsComponent,
    GeneralformComponent,
    SignUpComponent,
    LoginComponent,
    DocformComponent,
    ClassifiedComponent,
    WelcomComponent,
    ArticlesComponent,
    PostsComponent,
    TodosComponent,
    StationComponent,
    RekognitionComponent,
    RekdispComponent
  ],
  imports: [
    MatExpansionModule,
    MatCardModule,
    RouterModule.forRoot(
            appRoutes,
            { enableTracing: true } // <-- debugging purposes only
          ),  
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    AngularFireModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'test20-5b5e9'),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    HttpClientModule,
    MatDatepickerModule,
    MatSelectModule,
    MatNativeDateModule
  ],
  providers: [AngularFireAuth, AngularFirestore,DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }