import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { tap, catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GeneralsService {
//generalss: any =  [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]

userCollection:AngularFirestoreCollection = this.db.collection('users');
generalCollection:AngularFirestoreCollection



/*
addGenerals(){
  setInterval(() => 
    this.generals.push({title:'A new one', author:'New author'})
, 5000);    
}
*/


/*
getGenerals(): any {
  const generalsObservable = new Observable(observer => {
         setInterval(() => 
             observer.next(this.generals)
         , 5000);
  });  
  return generalsObservable;
}
*/
getGenerals(userId): Observable<any[]> {
  //const ref = this.db.collection('generals');
  //return ref.valueChanges({idField: 'id'});
  this.generalCollection = this.db.collection(`users/${userId}/generals`);
  console.log('Generals collection created');
  return this.generalCollection.snapshotChanges().pipe(
    map(collection => collection.map(document => {
      const data = document.payload.doc.data();
      data.id = document.payload.doc.id;
      return data;
    }))
  );    
} 


getGeneral(userId, id:string):Observable<any>{
  return this.db.doc(`users/${userId}/generals/${id}`).get();
}
/** for books
addGeneral(userId:string, title:string, author:string){
  console.log('In add generals');
  const general = {title:title,author:author}
  //this.db.collection('generals').add(general)  
  this.userCollection.doc(userId).collection('generals').add(general);
} 
*/
//for meetings
addGeneral(userId:string, title:string,/*author:string,*/date:Date,hall:string){
  console.log('In add generals');
  const general = {title:title,/*author:author,*/date:date, hall:hall }
  //this.db.collection('generals').add(general)  
  this.userCollection.doc(userId).collection('generals').add(general);
} 
/** for books
updateGeneral(userId:string, id:string,title:string,author:string){
  this.db.doc(`users/${userId}/generals/${id}`).update(
    {
      title:title,
      author:author
    }
  )
}
*/
//for meetings
updateGeneral(userId:string, id:string,title:string,/*author:string,*/date:Date,hall:string){
  this.db.doc(`users/${userId}/generals/${id}`).update(
    {
      title:title,
      date:date,
      hall:hall
    }
  )
}
deleteGeneral(userId:string, id:string){
  this.db.doc(`users/${userId}/generals/${id}`).delete();
}

constructor(private db: AngularFirestore,
            private authService:AuthService) {}
}