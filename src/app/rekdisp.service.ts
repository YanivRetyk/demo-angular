import { Rek } from './interfaces/rek';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RekdispService {  
  url = "http://ec2-18-188-71-144.us-east-2.compute.amazonaws.com/rekognition";
  public doc:string; 
  
  getRek(){
    return this.http.get<Rek[]>(this.url)
  }

  rekog():Observable<any>{
    console.log(this.doc);
    let json = {
      "Labels": [
        {
          "Name": this.doc
        }
      ]
    }
    let body  = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        return res;  
        console.log(res.body);
        let final = res.body.replace('[','');
        final = final.replace(']','');
        console.log(final);
        return final;      
      })
    );      
  }
  constructor(private http: HttpClient, 
    private db: AngularFirestore,
    private router:Router) { }
}
