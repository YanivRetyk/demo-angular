import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { Post } from './interfaces/post';
import { Photos } from './interfaces/photos';
import { Router } from '@angular/router';
import { Crime } from './interfaces/crime';
import { Force } from './interfaces/force';

@Injectable({
  providedIn: 'root'
})
export class PostsService {


  apiUrl='https://jsonplaceholder.typicode.com/posts';
  apiUrlpho='https://jsonplaceholder.typicode.com/todos';
  apiUrlCri='https://data.police.uk/api/forces';

  constructor(private http: HttpClient, 
    private db: AngularFirestore,
    private router:Router) { }
    
// GETING FROM JSON
getPosts(){
  return this.http.get<Post[]>(this.apiUrl)
}
getPhotos(){
  return this.http.get<Photos[]>(this.apiUrlpho)
}

getCrime(){
  return this.http.get<Crime[]>(this.apiUrlCri)
}
goto(id){
  return this.http.get<Force[]>('https://data.police.uk/api/forces/'+id) 
}

addPhoto(comp:boolean,  title:String){
  const todo = {title:title, completed:comp}
  this.db.collection('todos').add(todo)  
  .then(
    res =>  
    {
  this.router.navigate(['/todos']);
    }
  )
}  

}